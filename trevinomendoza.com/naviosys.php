<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//REM: Controller for 'System'  Aut: JLC  CodeLine: 60  Version: 1.0.6
class Naviosys extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->library("email");
	}



 	public function seisMeses()
	{
		$Comparacion = $this->db->query("SELECT *, DATEDIFF(vigencia_contrato, CURDATE()) AS diferencia FROM navio.cat_inmuebles")->result();
		foreach ($Comparacion as $value) {

		
			if ($value->diferencia == "178") {
				$imagen = "<img src='https://httpdocs/navio/assets/images/n_navio.png'/>";
				$datosArrendatario = $this->db->query("SELECT *, DATEDIFF(vigencia_contrato, CURDATE()) AS diferencia,
			     ctp.tipo_poliza as nombre_poliza,  cc.domicilio as calle_cliente FROM cat_inmuebles ci
		         left JOIN cat_frecuencia_renta cfr ON ci.frecuencia_renta = cfr.id_frecuencia
		         left JOIN cat_forma_pago cfp ON ci.forma_pago = cfp.id_forma_pago
		         left JOIN cat_clientes cc ON ci.id_cliente = cc.id_cliente
		         left JOIN cat_tarjetas ct ON ct.id_tarjeta = ci.nombre_banco
		         left JOIN cat_tpolizas ctp ON ci.tipo_poliza = ctp.id_poliza
		         left JOIN cat_plan cp ON ci.id_plan = cp.id_plan
		        
	             WHERE id_inmueble = '$value->id_inmueble'
		         
		         ;")->row();
				$correo = $datosArrendatario->correo;
				$nombre_cliente = $datosArrendatario->nombre_cliente;
				$inmueble_calle = $datosArrendatario->direccion;
				$inmueble_numero = $datosArrendatario->num_lugar;
				$inmueble_num_int = $datosArrendatario->cin_num_int;
				$inmueble_colonia = $datosArrendatario->colonia;
				$inmueble_ciudad = $datosArrendatario->cin_ciudad;
				$poliza = $datosArrendatario->id_inmueble;
				

				$configMail = array(
											'protocol' => 'smtp',
											'smtp_host' => 'smtp.ionos.mx',
											'smtp_port' => 587,
											'smtp_user' => 'info@navio.mx',
											'smtp_pass' => 'navio2018',
											'mailtype' => 'html',
											'charset' => 'utf-8',
											'newline' => "\r\n"
										);
				
				$this->email->initialize($configMail);
				$this->email->from('info@navio.mx', 'NAVIO - Blindaje Patrimonial.');
				$this->email->to("$correo");
				$this->email->subject('NAVIO - Credenciales de Acceso');
				$this->email->message("¡Hola, $nombre_cliente!,
										<br>
										<br>
										<br>
										Este es un correo electrónico de cortesía por medio del cual te informamos que tu inmueble
										<br>
										ubicado en $inmueble_calle $inmueble_numero $inmueble_num_int Col. $inmueble_colonia, $inmueble_ciudad, el cual está protegido por Navío por medio de la
										<br>
										Póliza número $poliza, ha llegado a sus primeros seis meses de vigencia.
										<br>
										<br>
										<br>
										<br>
										Agradecemos tu confianza y quedamos al pendiente de cualquier duda o comentario.
										<br>
										<br>
										<br>
										$imagen
	                                    <br>
	                                    www.navio.mx
	                                    <br>
	                                    Tel. 599 82 138
	                                    <br>");
				$this->email->send();
				var_dump($this->email->print_debugger());
			}
		}	
		
	}

		public function treintaDias()
		{
			$Comparacion = $this->db->query("SELECT *, DATEDIFF(vigencia_contrato, CURDATE()) AS diferencia FROM navio.cat_inmuebles")->result();
			foreach ($Comparacion as $value) {

			
				if ($value->diferencia == "29") {
					$imagen = "<img src='https://httpdocs/navio/assets/images/n_navio.png'/>";
					$datosArrendatario = $this->db->query("SELECT *, DATEDIFF(vigencia_contrato, CURDATE()) AS diferencia,
				     ctp.tipo_poliza as nombre_poliza,  cc.domicilio as calle_cliente FROM cat_inmuebles ci
			         left JOIN cat_frecuencia_renta cfr ON ci.frecuencia_renta = cfr.id_frecuencia
			         left JOIN cat_forma_pago cfp ON ci.forma_pago = cfp.id_forma_pago
			         left JOIN cat_clientes cc ON ci.id_cliente = cc.id_cliente
			         left JOIN cat_tarjetas ct ON ct.id_tarjeta = ci.nombre_banco
			         left JOIN cat_tpolizas ctp ON ci.tipo_poliza = ctp.id_poliza
			         left JOIN cat_plan cp ON ci.id_plan = cp.id_plan
			        
		             WHERE id_inmueble = '$value->id_inmueble'
			         
			         ;")->row();
					$correo = $datosArrendatario->correo;
					$nombre_cliente = $datosArrendatario->nombre_cliente;
					$inmueble_calle = $datosArrendatario->direccion;
					$inmueble_numero = $datosArrendatario->num_lugar;
					$inmueble_num_int = $datosArrendatario->cin_num_int;
					$inmueble_colonia = $datosArrendatario->colonia;
					$inmueble_ciudad = $datosArrendatario->cin_ciudad;
					$poliza = $datosArrendatario->id_inmueble;
					

					$configMail = array(
												'protocol' => 'smtp',
												'smtp_host' => 'smtp.ionos.mx',
												'smtp_port' => 587,
												'smtp_user' => 'info@navio.mx',
												'smtp_pass' => 'navio2018',
												'mailtype' => 'html',
												'charset' => 'utf-8',
												'newline' => "\r\n"
											);
					
					$this->email->initialize($configMail);
					$this->email->from('info@navio.mx', 'NAVIO - Blindaje Patrimonial.');
					$this->email->to("$correo");
					$this->email->subject('NAVIO - Credenciales de Acceso');
					$this->email->message("¡Hola, $nombre_cliente!,
											<br>
											<br>
											<br>
											Por medio del presente te informamos que la vigencia de tu inmueble ubicado en
											<br>
											$inmueble_calle $inmueble_numero $inmueble_num_int Col. $inmueble_colonia, $inmueble_ciudad, el cual está protegido por Navío por medio de la Póliza número
											<br>
											$poliza, está por terminar en menos de un mes.
											<br>
											<br>
											<br>
											Por lo tanto, te recomendamos ampliamente comunicarte a nuestros teléfonos de atención para
											<br>
											ofrecerte un gran precio por la renovación de tu Póliza para puedas continuar con la
											<br>
											tranquilidad que Navío te ofrece.
											<br>
											<br>
											<br>
											Agradecemos tu confianza y quedamos al pendiente de cualquier duda o comentario.
											<br>
											<br>
											<br>
											$imagen
		                                    <br>
		                                    www.navio.mx
		                                    <br>
		                                    Tel. 599 82 138
		                                    <br>");
					$this->email->send();
					var_dump($this->email->print_debugger());
				}
			}	
			
		}	


		public function dosDias()
		{
			$Comparacion = $this->db->query("SELECT *, DATEDIFF(vigencia_contrato, CURDATE()) AS diferencia FROM navio.cat_inmuebles")->result();
			foreach ($Comparacion as $value) {

			
				if ($value->diferencia == "2") {
					$imagen = "<img src='https://httpdocs/navio/assets/images/n_navio.png'/>";
					$datosArrendatario = $this->db->query("SELECT *, DATEDIFF(vigencia_contrato, CURDATE()) AS diferencia,
				     ctp.tipo_poliza as nombre_poliza,  cc.domicilio as calle_cliente FROM cat_inmuebles ci
			         left JOIN cat_frecuencia_renta cfr ON ci.frecuencia_renta = cfr.id_frecuencia
			         left JOIN cat_forma_pago cfp ON ci.forma_pago = cfp.id_forma_pago
			         left JOIN cat_clientes cc ON ci.id_cliente = cc.id_cliente
			         left JOIN cat_tarjetas ct ON ct.id_tarjeta = ci.nombre_banco
			         left JOIN cat_tpolizas ctp ON ci.tipo_poliza = ctp.id_poliza
			         left JOIN cat_plan cp ON ci.id_plan = cp.id_plan
			        
		             WHERE id_inmueble = '$value->id_inmueble'
			         
			         ;")->row();
					$correo = $datosArrendatario->correo;
					$nombre_cliente = $datosArrendatario->nombre_cliente;
					$inmueble_calle = $datosArrendatario->direccion;
					$inmueble_numero = $datosArrendatario->num_lugar;
					$inmueble_num_int = $datosArrendatario->cin_num_int;
					$inmueble_colonia = $datosArrendatario->colonia;
					$inmueble_ciudad = $datosArrendatario->cin_ciudad;
					$poliza = $datosArrendatario->id_inmueble;
					

					$configMail = array(
												'protocol' => 'smtp',
												'smtp_host' => 'smtp.ionos.mx',
												'smtp_port' => 587,
												'smtp_user' => 'info@navio.mx',
												'smtp_pass' => 'navio2018',
												'mailtype' => 'html',
												'charset' => 'utf-8',
												'newline' => "\r\n"
											);
					
					$this->email->initialize($configMail);
					$this->email->from('info@navio.mx', 'NAVIO - Blindaje Patrimonial.');
					$this->email->to("$correo");
					$this->email->subject('NAVIO - Credenciales de Acceso');
					$this->email->message("¡Hola, $nombre_cliente!,
											<br>
											<br>
											<br>
											Por medio del presente te informamos que la vigencia de tu inmueble ubicado en $inmueble_calle $inmueble_numero $inmueble_num_int Col. $inmueble_colonia, $inmueble_ciudad,
											<br>
											el cual está protegido por Navío por medio de la Póliza número
											<br>
											$poliza está por terminar.
											<br>
											<br>
											<br>
											Por lo tanto y en caso de aún no haber iniciado el proceso de renovación de tu Póliza, te
											<br>
											recomendamos ampliamente comunicarte a nuestros teléfonos de atención para ofrecerte un
											<br>
											gran precio por la renovación de tu Póliza para puedas continuar con la tranquilidad que Navío
											<br>
											te ofrece.
											<br>
											<br>
											<br>
											Agradecemos tu confianza y quedamos al pendiente de cualquier duda o comentario.
											<br>
											<br>
											<br>
											$imagen
		                                    <br>
		                                    www.navio.mx
		                                    <br>
		                                    Tel. 599 82 138
		                                    <br>");
					$this->email->send();
					var_dump($this->email->print_debugger());
				}
			}	
		}




}
